/**
Reader opens a file and analyzes it. Converting every x
chars into a class Chunk. Chunk will save a code representing
the chars it contains and an int indicating the line number
as well as one indicating the filename
**/
#IFNDEF READER_H
#DEFINE READER_H

#include <iostream>
#include <string>
#include <fstream>
#include <hash_set>
#include "Chunk.h"

using namespace std;

class Reader
{
private:
  hash_set <string,Chunk> chunks;
  string filename;
  int lineNumber;
  int charNumber;
  int chunkLineNumber;
  Chunk* makeChunk(string toChunk);
public:
  Reader();
  Reader(string file);
  hash_set <string,Chunk> readFile();
};
#endif
