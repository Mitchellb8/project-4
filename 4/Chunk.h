#ifndef CHUCK_H
#define CHUCK_H

#include <string>
using namespace std;

class Chunk
{
private:
  vector <int> line;
  long fileChars;
  string chunk;
  string makeChunk(string a);
public:
  Chunk();
  Chunk(long sizeOf, int l, string a);
  string getChunk();
  int getLine();
  bool twoLines();
} ;
#endif
